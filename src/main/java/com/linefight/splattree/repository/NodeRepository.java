package com.linefight.splattree.repository;

import com.linefight.splattree.entity.Node;
import org.springframework.data.repository.CrudRepository;

public interface NodeRepository extends CrudRepository<Node, Long> {
    Iterable<Node> findByParentId(long parentId);

    Node findByParentIdIsNull();
}
