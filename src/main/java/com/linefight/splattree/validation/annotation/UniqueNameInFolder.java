package com.linefight.splattree.validation.annotation;

import com.linefight.splattree.validation.UniqueNameInFolderValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = UniqueNameInFolderValidator.class)
@Documented
public @interface UniqueNameInFolder {
    String message() default "Node must have unique name in the folder";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
