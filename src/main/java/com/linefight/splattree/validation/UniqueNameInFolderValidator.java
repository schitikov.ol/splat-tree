package com.linefight.splattree.validation;

import com.linefight.splattree.entity.Node;
import com.linefight.splattree.service.NodeService;
import com.linefight.splattree.validation.annotation.UniqueNameInFolder;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class UniqueNameInFolderValidator implements ConstraintValidator<UniqueNameInFolder, Node> {

    private NodeService nodeService;

    @Autowired
    public UniqueNameInFolderValidator(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    @Override
    public void initialize(UniqueNameInFolder constraintAnnotation) {

    }

    @Override
    public boolean isValid(Node node, ConstraintValidatorContext context) {
        if (node.getParentId() == null)
            return true;

        List<Node> nodesInFolder = nodeService.getChildNodes(node.getParentId(), false);

        return nodesInFolder.stream()
                .filter(n -> !n.getId().equals(node.getId()))
                .map(Node::getName)
                .noneMatch(s -> s.equals(node.getName()));
    }
}
