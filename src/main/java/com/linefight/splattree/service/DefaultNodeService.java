package com.linefight.splattree.service;

import com.linefight.splattree.entity.Node;
import com.linefight.splattree.exception.ResourceNotFoundException;
import com.linefight.splattree.repository.NodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class DefaultNodeService implements NodeService {

    private NodeRepository nodeRepository;

    @Autowired
    public DefaultNodeService(NodeRepository nodeRepository) {
        this.nodeRepository = nodeRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Node> getAllNodes() {
        List<Node> nodes = new ArrayList<>();

        nodeRepository.findAll().forEach(nodes::add);

        return nodes;
    }

    @Override
    public Node createNode(Node node) {
        if (node.getParentId() == null)
            throw new ValidationException("Node must have parent id.");

        if (!nodeRepository.existsById(node.getParentId()))
            throw new ResourceNotFoundException("Node with id {" + node.getParentId() + "} not found.");

        return nodeRepository.save(node);
    }

    @Override
    public void updateNode(Node node) {
        if (node.getId() == null || !nodeRepository.existsById(node.getId()))
            throw new ResourceNotFoundException("Node with id {" + node.getId() + "} not found.");

        if (node.getParentId() == null || !nodeRepository.existsById(node.getParentId())) {
            throw new ResourceNotFoundException("Node with id {" + node.getParentId() + "} not found.");
        }

        nodeRepository.save(node);
    }

    @Override
    public void deleteNode(long nodeId) {
        if (!nodeRepository.existsById(nodeId))
            throw new ResourceNotFoundException("Node with id {" + nodeId + "} not found.");

        nodeRepository.deleteById(nodeId);
    }

    @Override
    @Transactional(readOnly = true)
    public Node getRootNode() {
        return nodeRepository.findByParentIdIsNull();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Node> getChildNodes(long parentId, boolean fakeLatency) {
        if (!nodeRepository.existsById(parentId))
            throw new ResourceNotFoundException("Node with id {" + parentId + "} not found.");

        List<Node> children = new ArrayList<>();

        nodeRepository.findByParentId(parentId).forEach(children::add);

        if (fakeLatency) {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return children;
    }

    @Override
    public Node getNode(long id) {
        return nodeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Node with id {" + id + "} not found."));
    }

}
