package com.linefight.splattree.service;

import com.linefight.splattree.entity.Node;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@Validated
public interface NodeService {

    @NotNull
    List<Node> getAllNodes();

    Node createNode(@Valid Node node);

    Node getNode(@Positive long id);

    void updateNode(@Valid Node node);

    void deleteNode(@Positive long nodeId);

    Node getRootNode();

    @NotNull
    List<Node> getChildNodes(@Positive long parentId, boolean fakeLatency);
}
