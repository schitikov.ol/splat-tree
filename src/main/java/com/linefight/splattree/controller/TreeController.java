package com.linefight.splattree.controller;

import com.linefight.splattree.entity.Node;
import com.linefight.splattree.service.NodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@RestController
public class TreeController {

    private NodeService nodeService;

    @Autowired
    public TreeController(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    @GetMapping("nodes")
    public List<Node> nodes() {
        return nodeService.getAllNodes();
    }

    @GetMapping("root")
    public Node root() {
        return nodeService.getRootNode();
    }

    @GetMapping("node/{id}/children")
    public List<Node> children(@PathVariable long id) {
        return nodeService.getChildNodes(id, true);
    }

    @PostMapping("node")
    @ResponseStatus(HttpStatus.CREATED)
    public Node createNode(@RequestBody Node node) {
        return nodeService.createNode(node);
    }

    @PutMapping("node")
    public void updateNode(@RequestBody Node node) {
        nodeService.updateNode(node);
    }

    @PatchMapping("node")
    public void patchNode(@RequestBody @Valid PatchForm patchForm) {
        Node node = nodeService.getNode(patchForm.getId());

        if (patchForm.getParentId() != null)
            node.setParentId(patchForm.getParentId());
        if (patchForm.getName() != null)
            node.setName(patchForm.getName());
        if (patchForm.getChildren() != null)
            node.setChildren(patchForm.getChildren());

        nodeService.updateNode(node);
    }

    @DeleteMapping("node/{id}")
    public void deleteNode(@PathVariable long id) {
        nodeService.deleteNode(id);
    }


    public static class PatchForm {

        @NotNull
        @Positive
        private Long id;

        @Positive
        private Long parentId;
        private String name;
        private List<Node> children;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Long getParentId() {
            return parentId;
        }

        public void setParentId(Long parentId) {
            this.parentId = parentId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Node> getChildren() {
            return children;
        }

        public void setChildren(List<Node> children) {
            this.children = children;
        }
    }
}
