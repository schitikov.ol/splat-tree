package com.linefight.splattree;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SplatTreeApplication {

    public static void main(String[] args) {
        SpringApplication.run(SplatTreeApplication.class, args);
    }

}
