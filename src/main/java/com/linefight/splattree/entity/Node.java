package com.linefight.splattree.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.linefight.splattree.validation.annotation.UniqueNameInFolder;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "NODE")

@UniqueNameInFolder //switch to this annotation if using not H2 database
public class Node {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @NotBlank
    private String name;

    @Column(name = "parent_id")
    private Long parentId;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    @OrderBy("name ASC")
    @Valid
    private List<Node> children;

    public Node(String name) {
        this.name = name;
    }

    public Node() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonIgnore
    public List<Node> getChildren() {
        return children;
    }

    @JsonProperty
    public void setChildren(List<Node> children) {
        this.children = children;
    }

    public void addChild(Node child) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(child);
    }

    public void removeChild(Node child) {
        if (children == null) {
            return;
        }
        children.remove(child);
    }

    public Node getChild(int index) {
        if (children == null) {
            return null;
        }

        return children.get(index);
    }

    public Long getId() {
        return id;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    public Long getParentId() {
        return parentId;
    }

    public interface ValidationOnSave {
    }

    public interface ValidationOnPatch {
    }
}
