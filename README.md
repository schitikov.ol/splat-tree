# Splat Tree Project

## Tools:
+ **Backend:** JDK 8, Spring Boot 2 (Web, Thymeleaf, Data JPA)
+ **Frontend:** Vue.js, Font Awesome, Twitter Bootstrap 4
+ **Servlet Container:** Embedded Tomcat
+ **Database:** H2 in-memory db
+ **IDE:** IntelliJ IDEA 2018
+ **Building:** Maven
+ Git

## Features:
+ The app represents a tree-like folder structure
+ CRUD operations
+ Lazy folders loading
+ Move nodes with drag&drop

## Building and start-up:

1) In the project root do the following in a console: `mvn clean package`
2) Then in the /target folder: `java -jar ./splat-tree-0.0.1-SNAPSHOT.jar`
3) App is deployed at `http://localhost:8080`

...Or just import project with IDEA :)

## Notes:
+ SQL ddl is located in the src/main/resources/create.sql. H2 is populated automatically with data.sql before app is running.
+ App was tested with Microsoft Edge 42.17134.1.0 and Google Chrome 69.0.3497.92

## Screenshots:
![](screenshots/tree.gif)